# Escribir un programa para leer a través de datos de una bandeja
# de entrada de correo y cuando encuentres una línea que comience
# con “From”, dividir la línea en palabras utilizando la función split.
# Estamos interesados en quién envió el mensaje, lo cual es la segunda
# palabra en las líneas que comienzan con From.
# From stephen.marquard@uct.ac.za Sat Jan 5 09:14:16 2008

nombre_archivo = input("Ingrese el nombre del archivo: ")
try:
    archivo = open(nombre_archivo)
except:
    print(f'Error al localizar el archivo {nombre_archivo}')
    exit()
cont = 0
for line in archivo:
    if not line.startswith('From'):
        continue
    if line.startswith('From:'):
        continue
    else:
        line = line.split()
        line = line[1]
        print(line)
    cont += 1
print("Hay", cont, "lineas en el archivo con la palabra From al inicio")

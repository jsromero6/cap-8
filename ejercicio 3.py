# Escribe un programa que pide al usuario una lista de
# números e imprime el máximo y el mínimo de los números al ﬁnal cuando
# el usuario ingresa “hecho”. Escribe el programa para almacenar los
# números que el usuario ingrese en una lista, y utiliza las funciones max()
# y min() para calcular el máximo y el mínimo después de que el bucle
# termine.


cont = 0
t = 0
lista = list()
while True:
    n = input('Ingrese un número: ')
    lista.append(n)
    if n.lower() in 'hecho':
        break
    try:
        t = t+float(n)
        cont += 1
        mayor = max(lista)
        mr = min(lista)
    except ValueError:
        print('Entrada inválida')
print('Máximo', mayor)
print('Mínimo', mr)